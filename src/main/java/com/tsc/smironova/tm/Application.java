package com.tsc.smironova.tm;

import com.tsc.smironova.tm.constant.TerminalConstant;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        parseArgs(args);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    public static void parseArg(final String arg) {
        if (TerminalConstant.ABOUT.equals(arg))
            showAbout();
        if (TerminalConstant.VERSION.equals(arg))
            showVersion();
        if (TerminalConstant.HELP.equals(arg))
            showHelp();
        if (TerminalConstant.EXIT.equals(arg))
            exit();
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0)
            return;
        final String arg = args[0];
        parseArg(arg);
        exit();
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Svetlana Mironova");
        System.out.println("E-MAIL: smironova@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConstant.ABOUT + " - Display developer info.");
        System.out.println(TerminalConstant.VERSION + " - Display program version.");
        System.out.println(TerminalConstant.HELP + " - Display list of terminal commands.");
        System.out.println(TerminalConstant.EXIT + " - Close application.");
    }

}
